module Try ( Try(..), Expr(..), eval, evalTry ) where

import Control.Applicative -- Otherwise you can't do the Applicative instance.
import Control.Monad (liftM, ap)

instance Functor Try where
  fmap = liftM

instance Applicative Try where
  pure  = return
  (<*>) = ap

data Try a = Err String
           | Return a
           deriving Show

instance Monad Try where
  return a        = Return a
  fail e          = Err e
  Return a >>= f  = f a
  Err e    >>= _  = Err e

data Expr = Lit Int | Div Expr Expr
  deriving Show

eval :: Expr -> Int
eval (Lit a) = a
eval (Div a b) = eval a `div` eval b

divTry :: Int -> Int -> Try Int
divTry a b = if b == 0
  then Err "Div by Zero"
  else Return (a `div` b)

evalTry :: Expr -> Try Int
evalTry (Lit a) = Return a
evalTry (Div a b) = do
  a' <- evalTry a
  b' <- evalTry b
  divTry a' b'
